package com.jspider.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.jspider.demo.entity.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long>{

	Registration findByemail(String email);
	
	@Query("from Registration where email=:email and id=:id")
	Registration findByEmailAndId(@Param("email") String email, @Param("id") Long id);
	
	List<Registration> findAllAndSortByNameInDesc();
}
