package com.jspider.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@Configuration @ComponentScan @EnableAutoConfiguration
public class UsmApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsmApplication.class, args);
	}

}
