package com.jspider.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jspider.demo.entity.Registration;
import com.jspider.demo.repository.RegistrationRepository;

@Service
public class RegistrationService {

	@Autowired
	private RegistrationRepository registrationRepository;

	public Registration saveRegistrationDetails(Registration registration) {
		return registrationRepository.save(registration);
	}

	public List<Registration> saveRegistrationList(List<Registration> registrations) {
		return registrationRepository.saveAll(registrations);
	}
	
	public List<Registration> getRegistrationDetails() {
		return registrationRepository.findAll();
	}
	
	public List<Registration> findAllAndSortByNameInDesc() {
		return registrationRepository.findAllAndSortByNameInDesc();
	}
	
	public Optional<Registration> getById(Long id) {
		return registrationRepository.findById(id);
	}
	
	public Registration getByEmail(String email) {
		return registrationRepository.findByemail(email);
	}
	
	public Registration getByEmailAndId(String email,Long id) {
		return registrationRepository.findByEmailAndId(email,id);
	}
	
	public void deleteById(Long id) {
		 registrationRepository.deleteById(id);
	}

}
