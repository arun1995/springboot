package com.jspider.demo.constants;

public interface IAppConstant {

	public static final String saveRegistrationDetails ="/saveRegistrationDetails";
	public static final String saveRegistrationList ="/saveRegistrationList";
	public static final String getRegistrationDetails ="/getRegistrationDetails";
	public static final String getRegistrationDetailsById ="/getRegistrationDetailsById";
	public static final String getRegistrationDetailsByEmail ="/getRegistrationDetailsByEmail";
	public static final String getByEmailAndId ="/getByEmailAndId";
	public static final String findAllAndSortByNameInDesc ="/findAllAndSortByNameInDesc";
	public static final String deleteRegistrationDetailsById ="/deleteRegistrationDetailsById";
	
}
