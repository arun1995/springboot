package com.jspider.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jspider.demo.AppConstants;
import com.jspider.demo.constants.IAppConstant;
import com.jspider.demo.entity.Registration;
import com.jspider.demo.service.RegistrationService;

/**
 * @author arun.rb215
 *
 */
@RestController
//@Controller
//@RequestMapping(value = "/register/*")
public class RegistrationController {

	@Autowired
	private RegistrationService registrationService;
	
	
	/**
	 * This method is used to save the user registration details
	 * @param registration of type Registration
	 * @return saved registration object
	 */
	@PostMapping(value = IAppConstant.saveRegistrationDetails)
	public Registration saveRegistrationDetails(@RequestBody Registration registration) {
		System.out.println(registration);
		return registrationService.saveRegistrationDetails(registration);
	}
	
	@PostMapping(value = IAppConstant.saveRegistrationList)
	public List<Registration> saveRegistrationList(@RequestBody List<Registration> registration) {
		System.out.println(registration);
		return registrationService.saveRegistrationList(registration);
	}
		
	
	@GetMapping(value = IAppConstant.getRegistrationDetails)
	public List<Registration> getRegistrationDetails() {
		return registrationService.getRegistrationDetails();
	}
	
	@GetMapping(value = IAppConstant.findAllAndSortByNameInDesc)
	public List<Registration> findAllAndSortByNameInDesc() {
		return registrationService.getRegistrationDetails();
	}
	
	@GetMapping(value = IAppConstant.getRegistrationDetailsById+"/{id}")
	public Optional<Registration>  getRegistrationDetailsById(@PathVariable("id")Long id) {
		return registrationService.getById(id);
	}
	
	
	@DeleteMapping(value = IAppConstant.deleteRegistrationDetailsById+"/{id}")
	public void  deleteRegistrationDetailsById(@PathVariable("id")Long id) {
		registrationService.deleteById(id);
	}
	
	@GetMapping(value = IAppConstant.getRegistrationDetailsByEmail)
	public Registration  getRegistrationDetailsByEmail(@RequestParam String email) {
		return registrationService.getByEmail(email);
	}
	
	@GetMapping(value = IAppConstant.getByEmailAndId)
	public Registration  getByEmailAndId(@RequestParam String email, @RequestParam Long id) {
		return registrationService.getByEmailAndId(email,id);
	}
	
	@GetMapping(value = "/test")
	public String test() {
		return "Application started";
	}
	
}
